<?php
	$title = 'What is Fracture Liaison Services | FLS Database Solutions | BoneMonitor';
	$description = 'BoneMonitor FLS Database Solution';
	include_once('public/head.php');

	$active = 'fls';
	include_once('public/header.php');
	include('public/fls-page.php');
	include_once('public/footer.php');
?>