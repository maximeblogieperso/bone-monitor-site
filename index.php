<?php
	$title = 'BoneMonitor | Homepage';
	$description = 'The BoneMonitor FLS Database Software';
	include_once('public/head.php');

	$active = 'home';
	include_once('public/header.php');
	include('public/front-page.php');
	include_once('public/footer.php');
?>