<section class=" generic-banner privacy--head">
	<div class="container text-center">
		<div class="max-width-lg centered">
			<h1 class="violet gradient-text title">
				Cookie Policy of bonemonitor.com
			</h1>
			<p>
				Cookies consist of portions of code installed in the browser that assist the Owner in providing the Service according to the purposes described. 
			</p>
		</div>
	</div>
</section>
<section class="privacy">
	<div class="container white--bg">
		<div class="text-wrapper">
			<h2>Cookie Policy of bonemonitor.com</h2>
		    <p>
		    	Cookies consist of portions of code installed in the browser that assist the Owner in providing the Service according to the purposes described. Some of the purposes for which Cookies are installed may also require the User's consent.</p>
		    <p>
		    	Where the installation of Cookies is based on consent, such consent can be freely withdrawn at any time following the instructions provided in this document.</p>

		    <h2>Technical Cookies and Cookies serving aggregated statistical purposes</h2>
		    <h3>Activity strictly necessary for the functioning of the Service</h3>

		    <p>This Website uses Cookies to save the User's session and to carry out other activities that are strictly necessary for the operation of this Website, for example in relation to the distribution of traffic.</p>

		    <h2>Activity regarding the saving of preferences, optimization, and statistics</h2>

		    <p>This Website uses Cookies to save browsing preferences and to optimize the User's browsing experience. Among these Cookies are, for example, those used for the setting of language and currency preferences or for the management of first party statistics employed directly by the Owner of the site.</p>

		    <h2>Other types of Cookies or third parties that install Cookies</h2>

		    <p>Some of the services listed below collect statistics in an anonymized and aggregated form and may not require the consent of the User or may be managed directly by the Owner - depending on how they are described - without the help of third parties.</p>

		    <p>If any third party operated services are listed among the tools below, these may be used to track Users’ browsing habits – in addition to the information specified herein and without the Owner’s knowledge. Please refer to the privacy policy of the listed services for detailed information.</p>

		    <ul>
		    	<li>Analytics</li>
		        <li>Displaying content from external platforms</li>
		        <li>Interaction with external social networks and platforms</li>
		        <li>SPAM protection</li>
		    </ul>

		    <h2>How to provide or withdraw consent to the installation of Cookies</h2>

		    <p>In addition to what is specified in this document, the User can manage preferences for Cookies directly from within their own browser and prevent – for example – third parties from installing Cookies.<br />
		    Through browser preferences, it is also possible to delete Cookies installed in the past, including the Cookies that may have saved the initial consent for the installation of Cookies by this website.</p>

		    <p>With regard to Cookies installed by third parties, Users can manage their preferences and withdrawal of their consent by clicking the related opt-out link (if provided), by using the means provided in the third party's privacy policy, or by contacting the third party.</p>

		    <p>Notwithstanding the above, the Owner informs that Users may follow the instructions provided on the subsequently linked initiatives by the EDAA (EU), the Network Advertising Initiative (US) and the Digital Advertising Alliance (US), DAAC (Canada), DDAI (Japan) or other similar services. Such initiatives allow Users to select their tracking preferences for most of the advertising tools. The Owner thus recommends that Users make use of these resources in addition to the information provided in this document.</p>

		    <h2>Owner and Data Controller</h2>

		    <p>BoneMonitor srl – Rue Zénobe Gramme 39 – 4821 Dison - Belgium<br/>

		    <p><b>Owner contact email:</b> <a href="mailto:info@bonemonitor.com" title="Contact us by email">info@bonemonitor.com</a></p>

		    <p>Since the installation of third-party Cookies and other tracking systems through the services used within this Website cannot be technically controlled by the Owner, any specific references to Cookies and tracking systems installed by third parties are to be considered indicative. In order to obtain complete information, the User is kindly requested to consult the privacy policy for the respective third-party services listed in this document.</p>

		    <p>Given the objective complexity surrounding the identification of technologies based on Cookies, Users are encouraged to contact the Owner should they wish to receive any further information on the use of Cookies by this Website.</p>

		    <h2>Legal information</h2>

		    <p><b>This cookie policy statement has been prepared based on provisions of multiple legislations, including Art. 13/14 of Regulation (EU) 2016/679 (General Data Protection Regulation).</b></p>

		    <p><i>Latest update: August 17, 2020</i></p>
		</div>
	</div>
</section>