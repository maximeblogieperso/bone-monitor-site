<?php
	// Contact page
?>	

<section class="contact">
	<div class="container-fluid">
		<div class="content-wrapper max-width-lg grey--bg">
			<div class="content max-width-md">
				<div class="title-wrapper">
					<div class="image-wrapper mobile-only thumbnail">
						<img src="assets/images/fls-square-image.jpg" alt="Bone Monitor medicin image">
					</div>
					<h1 class="title gradient-text">
						Contact us at <span>Bone Monitor</span> <small>in Belgium</small>
					</h1>
				</div>
				<p>
					Located near Liege, in Belgium, <strong>The Bone Monitor Team</strong> would be pleased to present you the FLS Database Solution. A live demo would help and you still have questions ? <strong>Feel free to ask and let's meet !</strong>
				</p>
				<?php include('public/contact-list.php'); ?>
			</div>
		</div>
	</div>
	<img src="assets/images/bone-monitor-medecin.jpg" class="img-fluid contact-image mobile-only" alt="Medecin professional image">
</section>