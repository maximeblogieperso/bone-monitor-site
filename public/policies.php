<section class=" generic-banner privacy--head">
	<div class="container text-center">
		<div class="max-width-lg centered">
			<h1 class="violet gradient-text title">
				Privacy Policy of BoneMonitor.com
			</h1>
			<p>This Website collects some Personal Data from its Users.</p>
		</div>
	</div>
</section>
<section class="privacy">
	<div class="container white--bg">
		<div class="text-wrapper">
			<h2>Owner and Data Controller</h2>
			<p>BoneMonitor srl – Rue Zénobe Gramme 39 – 4821 Dison - Belgium<br/>
			<b>Owner contact email:</b>
				<a href="mailto:info@bonemonitor.com" title="Contact BoneMonitor" target="_blank">
					info@bonemonitor.com
				</a>
			</p>
			<h2>
				Types of Data collected
			</h2>

			<p>Among the types of Personal Data that this Website collects, by itself or through third parties, there are: Cookies and Usage Data.
				<br /><br />
				Complete details on each type of data collected are provided in the dedicated sections of this privacy policy or by specific explanation texts displayed prior to the Data collection.<br />
				Usage Data is collected automatically when using this Website.<br />
				Any use of Cookies – or of other tracking tools – by this Website or by the owners of third-party services used by this Website serves the purpose of providing the Service required by the User, in addition to any other purposes described in the present document and in the Cookie Policy, if available.<br /><br />
				Users are responsible for any third-party Personal Data obtained, published or shared through this Website and confirm that they have the third party's consent to provide the Data to the Owner.
			</p>
			<h2>Mode and place of processing the Data</h2>
			<h3>Methods of processing</h3>
				<p>The Owner takes appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data.<br />
				The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Owner, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of this Website (administration, sales, marketing, legal, system administration) or external parties (such as third-party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Owner at any time.</p>
			<h2>Legal basis of processing</h2>
			<p>The Owner may process Personal Data relating to Users if one of the following applies:</p>
				<ul>
					<li>Users have given their consent for one or more specific purposes. Note: Under some legislations the Owner may be allowed to process Personal Data until the User objects to such processing (“opt-out”), without having to rely on consent or any other of the following legal bases. This, however, does not apply, whenever the processing of Personal Data is subject to European data protection law;</li>
					<li>provision of Data is necessary for the performance of an agreement with the User and/or for any pre-contractual obligations thereof;</li>
					<li>processing is necessary for compliance with a legal obligation to which the Owner is subject;</li>
					<li>processing is related to a task that is carried out in the public interest or in the exercise of official authority vested in the Owner;</li>
					<li>processing is necessary for the purposes of the legitimate interests pursued by the Owner or by a third party.</li>
				</ul>
				<p>
					In any case, the Owner will gladly help to clarify the specific legal basis that applies to the processing, and in particular whether the provision of Personal Data is a statutory or contractual requirement, or a requirement necessary to enter into a contract.
				</p>

			<h2>Place</h2>
			<p>
				The Data is processed at the Owner's operating offices and in any other places where the parties involved in the processing are located.
				<br /><br />Depending on the User's location, data transfers may involve transferring the User's Data to a country other than their own. To find out more about the place of processing of such transferred Data, Users can check the section containing details about the processing of Personal Data.
				<br /><br /> Users are also entitled to learn about the legal basis of Data transfers to a country outside the European Union or to any international organization governed by public international law or set up by two or more countries, such as the UN, and about the security measures taken by the Owner to safeguard their Data.
				<br /><br /> If any such transfer takes place, Users can find out more by checking the relevant sections of this document or inquire with the Owner using the information provided in the contact section.
			</p>

			<h2>Retention time</h2>
				<p>Personal Data shall be processed and stored for as long as required by the purpose they have been collected for.
				Therefore:</p>
				<ul>
					<li>Personal Data collected for purposes related to the performance of a contract between the Owner and the User shall be retained until such contract has been fully performed.</li>
					<li>Personal Data collected for the purposes of the Owner’s legitimate interests shall be retained as long as needed to fulfill such purposes. Users may find specific information regarding the legitimate interests pursued by the Owner within the relevant sections of this document or by contacting the Owner.</li>
				</ul>
			<p>
				The Owner may be allowed to retain Personal Data for a longer period whenever the User has given consent to such processing, as long as such consent is not withdrawn. Furthermore, the Owner may be obliged to retain Personal Data for a longer period whenever required to do so for the performance of a legal obligation or upon order of an authority.
				<br /><br />
				Once the retention period expires, Personal Data shall be deleted. Therefore, the right to access, the right to erasure, the right to rectification and the right to data portability cannot be enforced after expiration of the retention period.
			</p>
			<h2>The purposes of processing</h2>
				<p>The Data concerning the User is collected to allow the Owner to provide its Services, as well as for the following purposes: Analytics, Interaction with external social networks and platforms, Contacting the User, Displaying content from external platforms, SPAM protection and Tag Management.
				Users can find further detailed information about such purposes of processing and about the specific Personal Data used for each purpose in the respective sections of this document.
			</p>
			<h2>Detailed information on the processing of Personal Data</h2>
			<p>Personal Data is collected for the following purposes and using the following services:</p>
			<ul>
				<li>Analytics</li>
				<li>Contacting the User</li>
				<li>Displaying content from external platforms</li>
				<li>Interaction with external social networks and platforms</li>
				<li>SPAM protection</li>
			</ul>
			<h2>The rights of Users</h2>
				<p>Users may exercise certain rights regarding their Data processed by the Owner.</p>
				<p>In particular, Users have the right to do the following:</p>
				<ul>
					<li>Withdraw their consent at any time. Users have the right to withdraw consent where they have previously given their consent to the processing of their Personal Data.</li>
					<li>Object to processing of their Data. Users have the right to object to the processing of their Data if the processing is carried out on a legal basis other than consent. Further details are provided in the dedicated section below.</li>
					<li>Access their Data. Users have the right to learn if Data is being processed by the Owner, obtain disclosure regarding certain aspects of the processing and obtain a copy of the Data undergoing processing.</li>
					<li>Verify and seek rectification. Users have the right to verify the accuracy of their Data and ask for it to be updated or corrected.</li>
					<li>Restrict the processing of their Data. Users have the right, under certain circumstances, to restrict the processing of their Data. In this case, the Owner will not process their Data for any purpose other than storing it.</li>
					<li>Have their Personal Data deleted or otherwise removed. Users have the right, under certain circumstances, to obtain the erasure of their Data from the Owner.</li>
					<li>Receive their Data and have it transferred to another controller. Users have the right to receive their Data in a structured, commonly used and machine readable format and, if technically feasible, to have it transmitted to another controller without any hindrance. This provision is applicable provided that the Data is processed by automated means and that the processing is based on the User's consent, on a contract which the User is part of or on pre-contractual obligations thereof.</li>
					<li>Lodge a complaint. Users have the right to bring a claim before their competent data protection authority.</li>
				</ul>

			<h2>Details about the right to object to processing</h2>
				<p>
				Where Personal Data is processed for a public interest, in the exercise of an official authority vested in the Owner or for the purposes of the legitimate interests pursued by the Owner, Users may object to such processing by providing a ground related to their particular situation to justify the objection.
				</p>
				<p>Users must know that, however, should their Personal Data be processed for direct marketing purposes, they can object to that processing at any time without providing any justification. To learn, whether the Owner is processing Personal Data for direct marketing purposes, Users may refer to the relevant sections of this document.
				</p>
			<h2>How to exercise these rights</h2>
				<p>Any requests to exercise User rights can be directed to the Owner through the contact details provided in this document. These requests can be exercised free of charge and will be addressed by the Owner as early as possible and always within one month.</p>
				
			<h2>Cookie Policy</h2>
				<p>This Website uses Cookies. To learn more and for a detailed cookie notice, the User may consult the <a href="cookies.php" title="Cookie policy for BoneMonitor">Cookie Policy</a>.
				</p>
			
			<h2>Additional information about Data collection and processing</h2>
			<h3>Legal action</h3>
				<p>The User's Personal Data may be used for legal purposes by the Owner in Court or in the stages leading to possible legal action arising from improper use of this Website or the related Services.<br />
				The User declares to be aware that the Owner may be required to reveal personal data upon request of public authorities.
				</p>
				
			<h2>Additional information about User's Personal Data</h2>
				<p>In addition to the information contained in this privacy policy, this Website may provide the User with additional and contextual information concerning particular Services or the collection and processing of Personal Data upon request.</p>
			
			<h2>System logs and maintenance</h2>
				<p>
				For operation and maintenance purposes, this Website and any third-party services may collect files that record interaction with this Website (System logs) use other Personal Data (such as the IP Address) for this purpose.
				</p>
			<h3>Information not contained in this policy</h3>
				<p>
				More details concerning the collection or processing of Personal Data may be requested from the Owner at any time. Please see the contact information at the beginning of this document.
				</p>
			<h2>How “Do Not Track” requests are handled</h2>
				<p>
					This Website does not support “Do Not Track” requests. To determine whether any of the third-party services it uses honor the “Do Not Track” requests, please read their privacy policies.
				</p>
			<h2>Changes to this privacy policy</h2>
				<p>
				The Owner reserves the right to make changes to this privacy policy at any time by giving notice to its Users on this page and possibly within this Website and/or - as far as technically and legally feasible - sending a notice to Users via any contact information available to the Owner. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom.
				</p> 
				<p>Should the changes affect processing activities performed on the basis of the User’s consent, the Owner shall collect new consent from the User, where required.</p>
			<h2>Legal information</h2>
			<p>
				This privacy statement has been prepared based on provisions of multiple legislations, including Art. 13/14 of Regulation (EU) 2016/679 (General Data Protection Regulation).
				</p>
				<p>This privacy policy relates solely to this Website, if not stated otherwise within this document.</p>
				<!--Definitions and legal references-->
				<p><i>Latest update: May 24, 2018</i></p>
			</p>
		</div>
	</div>
</section>