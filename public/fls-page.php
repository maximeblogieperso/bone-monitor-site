<?php
	// FLS Page

	// Bullet points intro
	$bulletpoints = array(
		'Drives full-potential value from the collected datasets',
		'Measurable costs saving',
		'Innovative and self-learning solution for the assessment and monitoring of patients',
		'Key Performance Indicators to effectively improve your Fracture Liaison Service',
		'Easy to learn interfaces combined with strong technologies',
		'An eco-friendly solution for hospitals that saves paper'
		);

	// Quotes du slider 
	$quotes = array(
		'Develop a personalized plan for your patients',
		'Post fracture prevention management',
		'Avoid future bone breaks',
		'Help patients understand osteoporosis management',
		'Assistant to control the risk factors'
		);
?>

	<section id="fracture" class="two-col-content generic-banner pb-0">
		<div class="container">
			<div class="row">
				<div class="image-wrapper col-md-12 col-lg-5">
					<div class="sticky-top image-inner-wrapper">
						<img src="assets/images/fls-square-image.jpg" class="img-fluid" alt="Medical people using screen">
					</div>
				</div>
				<div class="text-wrapper col-md-12 col-lg-7 align-self-center">
					<h1 class="title big-title gradient-text">
						What are Fracture Liaison Services?
					</h1>
					<p>
					Fracture Liaison Services, commonly known as FLS, are coordinator-based, <strong>secondary fracture prevention services implemented by health care systems</strong>. The FLS enhances the communication between health care providers by providing a care pathway for the treatment of fragility fracture patients.
					</p>
					<p>
					The FLS will ensure all patients presenting with fragility fractures to the particular locality or institution receive fracture risk assessment and treatment where appropriate. The service will be comprised of a dedicated case worker, often a clinical nurse specialist, who works to pre-agreed protocols to case-find and assess fracture patients. The FLS can be based in secondary or primary care health care settings and requires support from a medically qualified practitioner, be they a hospital doctor with expertise in fragility fracture prevention or a primary care physician with a specialist interest.
					</p>

					<a href="#discover" class="btn-primary" title="Discover our solution">
						<i class="fa fa-caret-down"></i>
						Discover our solution
					</a>
				</div>
			</div> <!-- end row -->
		</div>
	</section>

	<section class="fls-intro generic-banner radial-violet--before">
		<div class="content-wrapper container">
			<div id="discover" class="schema-wrapper"> 
				<strong class="condensed h4">
					<span>1</span>
					Patients Hospitalized <br/>for fragility fractures
				</strong>
				<strong class="condensed h4 right-text">
					<span>2</span>
					Health Care <br/>Professionals
				</strong>
				<img src="assets/images/schema-bone-monitor.svg" class="img-fluid" alt="Bone Monitor schemas">
				<strong class="condensed h4">
					<span>3</span>
					General Practitioner
				</strong>
				<strong class="condensed h4 left-text">
					<span>4</span>
					Hospital Medical <br/>Team FLS
				</strong>
			</div>
			<div class="content">
				<h2 class="title big-title gradient-text">
					Why should you use <span>BoneMonitor® FLS Database?</span>
				</h2>
				<div class="row">
					<div class="col-md-5">
						<p>
							Our software has been developed in close partnership with deep domain experts to deliver a state-of-the-art solution for the <strong>data management of Fracture Liaison Services</strong>.
							The Monitoring System aims clinical work amelioration and stakeholders’ workflow betterment.
						</p>
					</div>
					<div class="col-md-7 listing-usp">
						<ul class="custom-list">
							<?php foreach ($bulletpoints as $point) { ?>
								<li>
							  		<?php echo $point; ?>
						    	</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="quotes" class="quotes-content">
		<div class="quotes container gradient-violet--bg">
			<div class="motif--bg content">
				<div class="text-wrapper max-width-xl centered">
					<h2 class="title white h1">
						Training materials and support
					</h2>
					<p class="white">
						We provide innovative training material packages composed of well-constructed training manuals and videos that includes all the relevant information. Do not hesitate to ask for our in-person training !
					</p>

					<a href="contact.php" class="btn-secondary" title="Get a demo of Bone Monitor">
						<i class="fa fa-caret-right"></i>
						Ask for a demo here
					</a>
				</div>
				<!-- Flickity HTML init -->
				<div class="carousel" data-flickity='{ "autoPlay": true, "contain": true, "prevNextButtons": false }'>
				<?php foreach ($quotes as $quote) { ?>
					<div class="carousel-cell quotes-item">
				  		<h3 class="condensed">
				  			<i><?php echo $quote; ?></i>
				  		</h3>
			    	</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</section>
