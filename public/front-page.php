 <?php
	// Main home part 
?>
	<section class="home gradient-before generic-banner">
		<div class="gradient-overlay"></div>
		<div class="content-wrapper container">
			<div class="content max-width-md white">
				<h1 class="title white">
					Welcome to <span>Bone Monitor</span> <small>FLS Database Software</small>
				</h1>
				<p>
					<strong>The BoneMonitor FLS Database Software</strong> will be your best partner to manage your prevention service and reduce the care gap, ensuring that fragility fractures sufferers receive appropriate assessment and monitoring to reduce future fracture risk.
				</p>
				<p>
					Our software has been developed in close partnership with deep domain experts to deliver a state-of-the-art solution for the <strong>data management of Fracture Liaison Services</strong>.
				</p>
				<div id="connexion" class="connexion-modal row">
					<div class="col-md-5 col-lg-5">
						<img src="assets/images/logo-bone-monitor-white.png" class="logo img-fluid" title="Bone monitor Logo">
					</div>
					<div class="col-md-7 col-lg-7">
						<form method="post">
							<input id="username" name="username" type="text" placeholder="Username"/>
							<input id="password" name="password" type="password" placeholder="Password"/>
							<label class="btn-submit" for"submit">
								<span class="btn-primary">
									<i class="fa fa-caret-right"></i> Connect
								</span>
								<input id="submit" name="submit" value="Submit" type="submit">
							</label>
							<a href="#" class="btn-small" title="Reset my password" target="_blank">
								I forgot my password
							</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
