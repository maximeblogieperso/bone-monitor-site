<ul class="contact-list">
	<li>
		<span class="icon">
			<i class="fa fa-map-marker"></i>
		</span>
  		Route Zénobe Gramme 39<br/>
  		4821 Dison<br/>
  		Belgium
	</li>
	<li>
		<a href="tel:+32 87 840 550" class="btn-link" title="Contact Bone Monitor by phone" rel="noreferrer">
			<span class="icon">
				<i class="fa fa-phone"></i>
			</span>
	  		+32 87 840 550
  		</a>
	</li>
	<li>
		<a href="mailto:info@bonemonitor.com?subject=Let's meet for a FLS Database demo" class="btn-link" title="Contact Bone Monitor by e-mail" rel="noreferrer">
			<span class="icon">
				<i class="fa fa-envelope-open-o"></i>
			</span>
	  		info@bonemonitor.com
  		</a>
	</li>
</ul>