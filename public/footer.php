<?php
	// Footer part 
?>
</main>
	<footer class="gradient-violet--bg">
		<div class="container">
			<div class="main-footer row">
				<div class="logo-wrapper col-md-12 col-xl-3">
					<img src="assets/images/logo-bone-monitor-white.png" class="logo img-fluid" title="Bone monitor Logo">
				</div>
				<div class="info-wrapper col-md-12 col-lg-8 col-xl-6">
					<h4 class="white">
						About Bone Monitor
					</h4>
					<p>
						The BoneMonitor FLS Database Software will be your best partner to manage your prevention service and reduce the care gap, ensuring that fragility fractures sufferers receive appropriate assessment and monitoring to reduce future fracture risk. Our software has been developed in close partnership with deep domain experts to deliver a state-of-the-art solution for the data management of Fracture Liaison Services.
					</p>
				</div>
				<div class="contact-wrapper col-md-12 col-lg-4 col-xl-3">
					<h4 class="white">
						Informations
					</h4>
					<?php include('public/contact-list.php'); ?>
				</div>
			</div>
			<div class="legal-footer row">
				<div class="legal-wrapper col-sm-8">
					<ul class="list-inline">
						<li class="list-inline-item">
							<a href="privacy-policy.php" class="btn-link white" title="Privacy Policy">
								Privacy Policy
							</a>
						</li>
						<li class="list-inline-item">
							<a href="cookies.php" class="btn-link white" title="Cookie Policy">
								Cookies Policiy
							</a>
						</li>
						<li class="list-inline-item">
							<span class="condensed white">
								Reg-Id: 0743.691.674 | VAT-Id: BE0743691674
							</span>
						</li>
					</ul>
				</div>
				<div class="copy-wrapper col-sm-4">
					<span class="white condensed">
						© Copyright 2020 - BoneMonitor
					</span>
				</div>
			</div>
		</div>
	</footer>
	<div id="cookieConsent">
	</div>
	</body>
</html>