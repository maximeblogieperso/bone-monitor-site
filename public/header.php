<?php
	// Nav and header
	$homeClass;
	$flsClass;
	$contactClass;

	$uri = $_SERVER['REQUEST_URI'];
	if( preg_match('/index/', $uri ) ) {
		$homeClass = 'active';
	}
	else if (preg_match('/FLS/', $uri) ) {
		$flsClass = 'active';
	} 
	else if ( preg_match('/contact/', $uri ) ) {
		$contactClass = 'active';
	}
?>
	<header>
		<div class="container">
			<nav class="main-nav row h-100">
				<div class="logo-wrapper col-sm-3">
					<a href="index.php" title="Go home">
						<img src="assets/images/logo-bone-monitor.png" class="img-fluid" alt="Bone Monitor Logo">
					</a>
				</div>
				<div class="menu-wrapper col-sm-9 align-self-center">
					<ul class="menu-list">
						<li>
							<a href="index.php" class="btn-link <?php echo $homeClass; ?>" title="Visit Bone Monitor homepage">
								Home
							</a>
						</li>
						<li>
							<a href="FLS-database.php" class="btn-link <?php echo $flsClass; ?>" title="Visit FLS Database page">
								FLS Database
							</a>
						</li>
						<li>
							<a href="contact.php" class="btn-link <?php echo $contactClass; ?>" title="Visite contact us page">
								Contact us
							</a>
						</li>
					</ul>
				</div>
				<a href="#" id="open-nav" class="btn-menu mobile-only" title="Acces to the menu">
					<i class="fa fa-bars"></i>
				</a>
			</nav>
		</div>
	</header>
	<nav id="nav" class="mobile-nav gradient-before">
		<div class="nav-wrapper">
			<a href="#" id="close-nav" class="btn-menu close-menu" title="Close the menu">
				<i class="fa fa-times"></i>
			</a>
			<div class="text-center">
				<span class="h3 title white d-block">
					Menu
				</span>
			</div>
			<ul class="menu-list">
				<li>
					<a href="index.php" class="condensed btn-link-mobile <?php echo $homeClass; ?>" title="Visit Bone Monitor homepage">
						Home
					</a>
				</li>
				<li>
					<a href="FLS-database.php" class="condensed btn-link-mobile <?php echo $flsClass; ?>" title="Visit FLS Database page">
						FLS Database
					</a>
				</li>
				<li>
					<a href="contact.php" class="condensed btn-link-mobile <?php echo $contactClass; ?>" title="Visite contact us page">
						Contact us
					</a>
				</li>
				<li class="pt-3">
					<a href="index.php#connexion" class="btn-primary" title="Visite contact us page">
						<i class="fa fa-caret-right"></i> Connect to Bone Monitor
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<main class="content">
