<?php
	$title = 'Contact BoneMonitor | FLS Database Solutions | BoneMonitor';
	$description = 'Contact BoneMonitor';
	include_once('public/head.php');

	$active = 'contact';
	include_once('public/header.php');
	include('public/contact-page.php');
	include_once('public/footer.php');
?>